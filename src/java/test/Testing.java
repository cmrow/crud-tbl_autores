/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import co.edu.sena.crudAutores.presistencia.dao.AutorDAO;
import co.edu.sena.crudAutores.presistencia.vo.AutorVO;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class Testing {

    public static void consultarAutores() throws Exception {
        List<AutorVO> listaAutores = AutorDAO.consultar();
        for (AutorVO autorVO : listaAutores) {

            System.out.println(autorVO.getId());
            System.out.println(autorVO.getNombre());
            System.out.println(autorVO.getEspecialidad());
        }
    }

    public static boolean actualizarAutor(AutorVO autorActual) throws Exception {
        boolean flagAutor = AutorDAO.actualizar(autorActual);
        return flagAutor;

    }

    public static void agregarAutor(AutorVO autor) throws Exception {

    }

    public static void main(String[] args) throws Exception {
        boolean flag;
        String nombre = "ActualizadoNombre", especialidad = "ActualizadoEspecialidad";
        int id = 1;
        AutorVO autorActual = new AutorVO(nombre,especialidad,id);
        flag = Testing.actualizarAutor(autorActual); //Testing.agregarAutor(autorNuevo);
        //Testing.consultarAutores();
        System.out.println(flag);

    }
}
