/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.crudAutores.persistencia.conexion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Carlos
 */
public class Conexion {
    
    
    public static Connection cnn = null;
    
    public static Connection conectar() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/biblioteca", "root", "");    
            return cnn;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
    }

    /**
     *
     * @param ps
     * @param rs
     * @param cnn
     */
    public static void cerrar(PreparedStatement ps, ResultSet rs, Connection cnn, CallableStatement cs) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
            if (cs != null) {
                cs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param cnn
     */
    public static void cerrar(Connection cnn) {
        cerrar(null, null, cnn, null);
    }

    /**
     *
     * @param ps
     * @param rs
     */
    public static void cerrar(PreparedStatement ps, ResultSet rs) {
        cerrar(ps, rs, null, null);
    }

    /**
     *
     * @param ps
     * @param rs
     */
    public static void cerrar(CallableStatement cs) {
        cerrar(null, null, null, cs);
    }
    
}
