/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.crudAutores.servlets;

import co.edu.sena.crudAutores.presistencia.dao.AutorDAO;
import co.edu.sena.crudAutores.presistencia.vo.AutorVO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Carlos
 */
@WebServlet(name = "AutoresServlet", urlPatterns = {"/AutoresServlet"})
public class AutoresServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AutoresServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AutoresServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        String nombre = request.getParameter("nombre");
        String especialidad = request.getParameter("especialidad");
        try {
            AutorVO autorNuevo = new AutorVO();
            autorNuevo.setNombre(nombre);
            autorNuevo.setEspecialidad(especialidad);
            boolean flag = AutorDAO.agregar(autorNuevo);
            response.sendRedirect("index.jsp");

        } catch (Exception e) {
        }

    }

}
