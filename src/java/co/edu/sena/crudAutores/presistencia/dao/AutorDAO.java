/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.crudAutores.presistencia.dao;

import co.edu.sena.crudAutores.persistencia.conexion.Conexion;
import static co.edu.sena.crudAutores.persistencia.conexion.Conexion.cnn;
import co.edu.sena.crudAutores.presistencia.vo.AutorVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class AutorDAO {

    public static CallableStatement cs = null;
    public static ResultSet rs = null;
    public static List<AutorVO> lista = new ArrayList<>();

    public static List<AutorVO> consultar() throws Exception {
        try {
            cnn = Conexion.conectar();
            cs = cnn.prepareCall("{call pa_consultar_autores}");
            rs = cs.executeQuery();
            while (rs.next()) {
                AutorVO autor = new AutorVO(rs.getInt("id"));
                autor.setNombre(rs.getString("nombre"));
                autor.setEspecialidad(rs.getString("especialidad"));
                lista.add(autor);
            }
            Conexion.cerrar(cnn);
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
    }

    public static AutorVO consultarId(int idAutor) throws Exception {
        try {
            cnn = Conexion.conectar();
            cs = cnn.prepareCall("{call pa_consultar_id_autor(?)}");
            cs.setInt(1, idAutor);
            rs = cs.executeQuery();
            rs.next();
            AutorVO autor = new AutorVO(rs.getInt("id"));
            autor.setNombre(rs.getString("nombre"));
            autor.setEspecialidad(rs.getString("especialidad"));
            Conexion.cerrar(cnn);
            return autor;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
    }

    public static boolean agregar(AutorVO autor) throws Exception {
        boolean flagAgregar = false;
        String nombre, especialidad;
        nombre = autor.getNombre();
        especialidad = autor.getEspecialidad();
        try {
            cnn = Conexion.conectar();
            cs = cnn.prepareCall("{call biblioteca.pa_agregar_autor(?,?)}");
            cs.setString(1, nombre);
            cs.setString(2, especialidad);
            cs.execute();
            flagAgregar = true;
            Conexion.cerrar(cnn);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
        return flagAgregar;

    }

    public static boolean actualizar(AutorVO autorAc) throws Exception {
        boolean flagActualizar = false;
        try {
            cnn = Conexion.conectar();
            cs = cnn.prepareCall("call biblioteca.pa_actualizar_autor(?, ?, ?);");
            cs.setString(1, autorAc.getNombre());
            cs.setString(2, autorAc.getEspecialidad());
            cs.setInt(3, autorAc.getId());
            cs.execute();
            flagActualizar = true;
            Conexion.cerrar(cnn);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al conectar a la bd");
        }
        return flagActualizar;
    }

}
