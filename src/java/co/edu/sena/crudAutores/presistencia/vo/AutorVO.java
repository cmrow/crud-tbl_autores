/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.crudAutores.presistencia.vo;

/**
 *
 * @author Carlos
 */
public class AutorVO {
    private String nombre, especialidad;
    private Integer id;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AutorVO(String nombre, String especialidad, Integer id) {
        this.nombre = nombre;
        this.especialidad = especialidad;
        this.id = id;
    }

    public AutorVO(Integer id) {
        this.id = id;
    }

    public AutorVO() {
    }

    public AutorVO(String nombre, String especialidad) {
        this.nombre = nombre;
        this.especialidad = especialidad;
    }
    
    
}
